#!/usr/bin/env sh

DIR_SELF=$(cd "$(dirname "$0")" || exit 2; pwd)
OUTPUT=${OUTPUT:-"$DIR_SELF/output"}
INPUT=${INPUT:-"$DIR_SELF/input.txt"}

################################################################

[ "$(id -un)" != "root" ] && {
    (>&2 printf "|| run as root\n")
    exit 1
}

[ ! -f "$INPUT" ] && {
    touch "$INPUT"
    (>&2 printf "++ created '%s' file\n" "$INPUT")
}
[ ! -d "$OUTPUT" ] && {
    mkdir -p "$OUTPUT"
    (>&2 printf "++ created '%s' folder\n" "$OUTPUT")
}

if ! command -v file > /dev/null 2>&1; then
    (>&2 printf "|| missing 'file' command\n")
    exit 1
fi
if ! command -v diff > /dev/null 2>&1; then
    (>&2 printf "|| missing 'diff' command\n")
    exit 1
fi

################################################################

texttype() {
    case $(file -b0 --mime-type "$1") in
        text/*)             return 0 ;;
        application/json*)  return 0 ;;
    esac
    return 1
}

validate() {
    if [ ! -f "$1" ]; then
        (>&2 printf "|| not there %s\n" "$1")
        return
    fi
    real=$(realpath "$1")
    if ! texttype "$real"; then
        (>&2 printf "|| not text %s\n" "$1")
        return
    fi
    echo "$real"
}

relocate() {
    case $1 in
        */.git/*)
            echo "$OUTPUT$1" | sed 's|/.git/|/_git/|g'  ;;
        *)  echo "$OUTPUT$1"                            ;;
    esac
}

handling() {
    source=$(validate "$1")
    if [ -n "$source" ]; then
        target=$(relocate "$source")
        parent=$(dirname "$target")
        [ ! -d "$parent" ] && mkdir -p "$parent"
        if [ -f "$target" ]; then
            tdiff=$(diff "$target" "$source")
            [ -n "$tdiff" ] && (>&2 printf ">< %s\n%s\n" "$source" "$tdiff")
        else
            (>&2 printf "++ %s\n" "$source")
        fi
        cp -p "$source" "$target"
    fi
}

################################################################

append() {
    bucket=""
    content=$(cat "$INPUT")

    for elem in "$@"; do
        source=$(validate "$elem")
        if [ -n "$source" ]; then
            if ! echo "$content" | grep -w "$source" > /dev/null; then
                (>&2 printf "++ %s\n" "$source")
                bucket=$(printf "%s\n%s" "$bucket" "$source")
            fi
        fi
    done

    {
        echo "$bucket"
        echo "$content"
    } | grep '\S' | sort -u -o "$INPUT"
}

backup() {
    pids=""

    while read -r elem; do
        (handling "$elem") &
        pids="$pids $!"
    done < "$INPUT"

    for elem in $pids; do wait "$elem"; done
}

################################################################

if [ -n "$*" ]; then
    append "$@"
else
    backup
fi

exit 0
