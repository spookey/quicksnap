# quicksnap

> track changes from a tree of text files and copy them over
>
> useful to store system configuration in a git repository

## bootstrap

Clone the repo somewhere safe
— for example inside the home folder of `user` being that user:
`/usr/home/user/code/quicksnap`.

To be able to properly access & copy all files `quicksnap` must be
run as `root` user.

Create some working directory as such:
`/root/config_store`.

Highly recomended
— initialize a `git` repository in there:

```sh
git -C /root/config_store init
```

Write yourself some wrapper script inside some folder that
is part of `$PATH` environment variable.
Also don't forget to make it executable (`chmod +x`).
For example in
`/root/bin/quicksnap`:

```sh
#!/usr/bin/env sh

INPUT="/root/config_store/input.txt" \
OUTPUT="/root/config_store/output" \
  /usr/home/user/code/quicksnap/quicksnap.sh "$@"
```

## usage

- Use `quicksnap` _with arguments_ to add to `input.txt`.

  The real path will be resolved:

  ```sh
  cd /some/weird/path
  quicksnap file.json
  ```

  Adds this to `input.txt`:

  ```plain
  /some/weird/path/file.json
  ```

  The entries in `input.txt` will be sorted alphabetically.

- Use `quicksnap` _without arguments_ to copy files from `input.txt`
  to the `output` folder.
